﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;


namespace kata_babysitter
{
    [TestFixture]
    public class UnitTest1
    {
        [Test]
        public void BabySitterCreditedForOneHourBeforeBedtime()
        {
            BabySitter babySitter = new BabySitter(5, 6, 10);
            Assert.AreEqual(1, babySitter.calculateHoursBeforeBedtime());
        }

        [Test]
        public void BabySitterCreditedForOneHourAfterBedtimeBeforeMidnight()
        {
            BabySitter babySitter = new BabySitter(11, 12, 10);
            Assert.AreEqual(1, babySitter.calculateHoursAfterBedtimeBeforeMidnight());
        }

        [Test]
        public void BabySitterCreditedForOneHourAfterMidnight()
        {
            BabySitter babySitter = new BabySitter(11, 13, 10);
            Assert.AreEqual(1, babySitter.calculateHoursAfterMidnight());
        }

        [Test]
        public void BabySitterCreditedForOneFullNight()
        {
            BabySitter babySitter = new BabySitter(5, 16, 10);
            Assert.AreEqual(5, babySitter.calculateHoursBeforeBedtime());
            Assert.AreEqual(2, babySitter.calculateHoursAfterBedtimeBeforeMidnight());
            Assert.AreEqual(4, babySitter.calculateHoursAfterMidnight());
        }

        [Test]
        public void BabySitterPaidForOneHourBeforeBedtime()
        {
            BabySitter babySitter = new BabySitter(5, 6, 10);
            Assert.AreEqual(12, babySitter.getBeforeBedtimePay());
        }

        [Test]
        public void BabySitterPaidForOneHourAfterBedtimeBeforeMidnight()
        {
            BabySitter babySitter = new BabySitter(11, 12, 10);
            Assert.AreEqual(8, babySitter.getAfterBedtimeBeforeMidnightPay());
        }

        [Test]
        public void BabySitterPaidForOneHourAfterMidnight()
        {
            BabySitter babySitter = new BabySitter(11, 13, 10);
            Assert.AreEqual(16, babySitter.getAfterMidnightPay());
        }

        [Test]
        public void BabySitterPaidForOneFullNight()
        {
            BabySitter babySitter = new BabySitter(5, 16, 10);
            Assert.AreEqual(5, babySitter.calculateHoursBeforeBedtime());
            Assert.AreEqual(2, babySitter.calculateHoursAfterBedtimeBeforeMidnight());
            Assert.AreEqual(4, babySitter.calculateHoursAfterMidnight());
            Assert.AreEqual(140, babySitter.calculateNightsPay());
        }

        [Test]
        public void BabySitterPaidForOneRandomNight()
        {
            BabySitter babySitter = new BabySitter(7, 14, 13);
            Assert.AreEqual(92, babySitter.calculateNightsPay());
        }

        [Test]
        public void BabySitterPaidForOneRandomNight2()
        {
            BabySitter babySitter = new BabySitter(13, 16, 10);
            Assert.AreEqual(0, babySitter.calculateHoursBeforeBedtime());
            Assert.AreEqual(0, babySitter.calculateHoursAfterBedtimeBeforeMidnight());
            Assert.AreEqual(3, babySitter.calculateHoursAfterMidnight());
            Assert.AreEqual(48, babySitter.calculateNightsPay());
        }
    }
}